bed2bigWig
================
Carson Callahan
August 22, 2019

Intro
-----

Me and a friend were having a look at some GEO data that was only deposited as beds, but we wanted to view the data on IGV... so we needed to figure out how to convert the files. The path we'll be taking for this is BED -&gt; sorted+indexed BAM -&gt; bigWig. Tools required:

-   samtools
-   bedtools
-   deepTools

NB: These are bed files converted from BAMs initially, *not* MACS peak files (which are also bed files).

Code
----

I've included a script in this repo that can automate the process for you, using the method outlined below. Requirements:

-   You have all the tools I mentioned above
-   All you beds you'd like to convert are in the same directory
-   You have access to a chromosome sizes file - [here](http://hgdownload.cse.ucsc.edu/goldenPath/hg19/bigZips/hg19.chrom.sizes) is a link to the UCSC hg19 sizes

Usage is as follows:

``` bash
bed2bigWig.sh -d path/to/bed/directory -g path/to/chromosome/sizes
```

The script is set up to require both the `-d` and `-g` options. You can also type `bed2bigWig.sh -h` for help. I haven't included options for modifying the bedtools and deeptools parameters, but these can be directly edited in the script itself if you wish.

Step by step breakdown
----------------------

First, we have to invent a new column to get the bed to play nicely with our tools.

``` bash
cat test.bed | awk '{x++; printf "%s\tread%d\n",$0,x}' > new_test.bed
```

Next, convert the bed file to a BAM and sort it.

``` bash
bedToBam -i /path/to/new_test.bed -g /path/to/chrom/sizes | samtools sort -@ 5 -m 1G  -T ${prefix}.temp -o new_test_bed_sorted.bam
```

Next, we need to index the file (deepTools will require this later on).

``` bash
samtools index new_test_bed_sorted.bam
```

Lastly, convert the sorted and indexed bam into a bigwig using deepTools.

``` bash
bamCoverage -b new_test_bed_sorted.bam --normalizeUsing RPKM --binSize 30 --smoothLength 300 -p 5 --extendReads 200 -o new_test_bed.bigwig
```

And there you have it - your file should be ready to load into IGV for viewing.
