#!/usr/bin/env bash

#######################################################
## Author: Carson Callahan
## Purpose: Convert bedGraphs to bigWigs
## Date: 2019-08-09
## Notes: 1) requires samtools, bedtools, and deeptools
## 			2) requires a file of chromosome sizes
#######################################################


#### Set options ####
while getopts ":dgh" opt; do
	case $opt in
		d) dir=("$OPTARG")
			;;

		g) chromsize=("$OPTARG")
			;;

		h) echo "Usage: $(basename $0) [-d path/to/bed/folder] [-g path/to/chromfile]"
			echo "Script requires samtools, bedtools, and deepTools"
			exit 1
			;;

		*) echo "Unrecognized command; use the -h flag for help"
			echo "Usage: $(basename $0) [-d path/to/bed/folder] [-g path/to/chromfile]"
			exit 1
			;;
	esac
done
shift $((OPTIND -1))

## See how optarg is numbering our flags...
# echo "${1} ${3}"
## Set an if statement to make sure flags are provided
if [ ! "${1}" ] || [ ! "${3}" ]; then
	echo "Argument missing! Usage: $(basename $0) [-d path/to/bed/folder] [-g path/to/chromfile]"
	exit 1
fi

#### Other Arguments ####
pwd=`pwd`

#### Code ####
## Make some nice print statements
COUNTER=1
cd ${1}
num=$(ls *.bed|echo `wc -l`|awk '{print $1}')
echo There are ${num} bed files to convert

## Make the loop
for file in *.bed; do
	echo Working on file ${COUNTER}...
	cat ${file} | awk '{x++; printf "%s\tread%d\n",$0,x}' > ${file%.bed}_formatted.bed
	bedToBam -i ${file%.bed}_formatted.bed -g ${3} | samtools sort -@ 5 -m 1G  -T ${prefix}.temp -o ${file%.bed}_sorted.bam
	samtools index ${file%.bed}_sorted.bam
	echo "Converting to bigWig... this will take a few minutes"
	bamCoverage -b ${file%.bed}_sorted.bam --normalizeUsing RPKM --binSize 30 --smoothLength 300 -p 5 --extendReads 200 -o ${file%.bed}.bigwig
	echo Finished converting file number ${COUNTER} to bigWig
	let COUNTER=COUNTER+1
done








